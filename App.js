import React from "react"
import Buttons from "./Screen/buttons"
import { NavigationContainer, DefaultTheme } from "@react-navigation/native"
import {
	DrawerItemList,
	createDrawerNavigator,
	DrawerContentScrollView,
} from "@react-navigation/drawer"
import Info from "./Screen/info"
import Rigolo from "./Screen/rigolo"
import Coronavirus from "./Screen/corona"
import {
	Image,
	View,
	Linking,
	StyleSheet,
	TouchableNativeFeedback,
} from "react-native"


function CustomDrawerContent(props) {
	return (
		<DrawerContentScrollView {...props}>
			<View style={{borderBottomWidth: 1, borderBottomColor: "#383838",}}>
				<TouchableNativeFeedback onPress={() => Linking.openURL("https://cjaac.netlify.com")}>
					<Image style={label.drawer} source={require("./Média/devices.png")} />
				</TouchableNativeFeedback>
			</View>
			<View style={{marginTop: 10}} />
			<DrawerItemList labelStyle={label.label} {...props} />
		</DrawerContentScrollView>
	)
}

const label = StyleSheet.create({
	label: {
		color: "#125a82",
		fontFamily: "Manjari-Bold",
		fontSize: 20
	},

	drawer: {
		height: 130,
		width: 130,
		marginLeft: "auto",
		marginRight: "auto",
		marginTop: 30,
		marginBottom: 20,
	}
})


const MyTheme = {
	...DefaultTheme,
	colors: {
		...DefaultTheme.colors,
		primary: "rgb(255, 45, 85)",
	},
}

const Drawer = createDrawerNavigator()

function MyDrawer() {
	return (
		<Drawer.Navigator
			drawerContent={props => CustomDrawerContent(props)}
			drawerStyle={{
				width: 240,
				backgroundColor: "#202125",
			}}
		>
			<Drawer.Screen
				options={{
					drawerIcon:() => { return <View><Image
						source={require("./Média/speaker.png")}
						style={{height: 40, width: 40, marginRight: -10}}
					/></View>
					}}} name="SoundBox" component={Buttons}
			/>

			<Drawer.Screen
				options={{
					drawerIcon:() => { return <View><Image
						source={require("./Média/rigolo.png")}
						style={{height: 40, width: 40, marginRight: -10}}
					/></View>
					}}} name="Rigolo" component={Rigolo}
			/>

			<Drawer.Screen
				options={{
					drawerIcon:() => { return <View><Image
						source={require("./Média/coronavirus/Coro-rouge.png")}
						style={{height: 40, width: 40, marginRight: -10}}
					/></View>
					}}} name="Coronavirus" component={Coronavirus}
			/>

			<Drawer.Screen
				options={{
					drawerIcon:() => { return <View><Image
						source={require("./Média/info.png")}
						style={{height: 40, width: 40, marginRight: -10}}
					/></View>
					}}} name="Information" component={Info}
			/>
			
		</Drawer.Navigator>
	)
}

export default function App() {
	return (
		<NavigationContainer theme={MyTheme}>
			<MyDrawer />
		</NavigationContainer>
	)
}