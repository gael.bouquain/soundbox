import React from "react";
import {
    View,
    Text,
    StyleSheet,
    Linking,
    Image,
    StatusBar,
    TouchableNativeFeedback,
} from "react-native";


class Info extends React.Component {
    render() {
        return (
            <View style={{
                    flex: 1,
                    backgroundColor: "#202125"
                }}>
                <StatusBar hidden={true}/>
                <TouchableNativeFeedback onPress={() => this.props.navigation.openDrawer()}>
                        <Image style={{height: 35, width: 35, position: 'absolute', left: 20, top: 20}} source={require('../Média/menu-icon.png')} />
                </TouchableNativeFeedback>
                <View style={{alignItems: "center", marginTop: 60}}>
                    <Text style={[style.Text, {}]}>Ce logiciel à été dévlopper par</Text>

                    <Text style={[style.Text, {marginTop: 8, fontSize: 26}]}>Gaël Bouquain</Text>

                    <Text style={[style.Text, {marginTop: 8, fontSize: 23}]}>Le site  du créateur :</Text>
                    <Text
                    onPress={() => Linking.openURL("https://cjaac.netlify.com")}
                    style={[style.Text, {
                        fontSize: 25,
                        marginTop: 6,
                        textDecorationLine: "underline"
                    }]}>cjaac.netlify.com</Text>
                        <Image style={{height: 121, width: 132, marginTop: 20}} source={require("../Média/posa.png")} />
                    <Text style={[style.Text, {marginTop: 20}]}>logiciels utilisés</Text>
                    <Text onPress={() => Linking.openURL("https://reactnative.dev/")} style={[style.Text, {marginTop: 20, textDecorationLine: "underline"}]}>React Native</Text>
                    <Text onPress={() => Linking.openURL("https://github.com/zmxv/react-native-sound")} style={[style.Text, {marginTop: 10, textDecorationLine: "underline"}]}>react-navigation</Text>
                    <Text onPress={() => Linking.openURL("https://reactnavigation.org/")} style={[style.Text, {marginTop: 10, textDecorationLine: "underline"}]}>react-native-sound</Text>
                        <Text style={[style.Text, {marginTop: 20, fontSize: 13}]}>Icônes réalisées par Freepik à partir de www.flaticon.com</Text>
                    <Text style={[style.Text, {marginTop: 1, color: "black"}]}>Coronavirus edition</Text>

                </View>
            </View>
        )
    }
}


const style = StyleSheet.create({
    Text: {
        fontFamily: "Manjari-Bold",
        color: "#7d9ce8",
        fontSize: 19,
    }
})


export default Info
