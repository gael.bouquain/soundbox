import React from "react"
import {
	View,
	Text,
	Image,
	StatusBar,
	StyleSheet,
	ToastAndroid,
	TouchableNativeFeedback,
} from "react-native"
import AwesomeButtonRick from "react-native-really-awesome-button/src/themes/rick"
import Clipboard from "@react-native-community/clipboard"

let list = [
	"ᓚᘏᗢ",
	"༼ つ ◕_◕ ༽つ",
	"(¬‿¬))",
	"(⌐■_■))",
	"(☞ﾟヮﾟ)☞)",
	"¯\\_(ツ)_/¯",
	"(╯°□°）╯︵ ┻━┻)",
	"(T_T))",
	"(⊙_⊙;))",
	"(┬┬﹏┬┬))",
	"╰(*°▽°*)╯",
	"┌(。Д。)┐",
	"(ˉ﹃ˉ))",
	"...(*￣０￣)ノ",
	"(⊙_(⊙_⊙)_⊙))",
	"＼（〇_ｏ）／",
	"Σ(っ °Д °;)っ",
	"╰（‵□′）╯",
]

class Rigolo extends React.Component {
	copy(value) {
		Clipboard.setString(value)
		ToastAndroid.show(
			`Tout est good\nTu as bien copier ${value}`,
			ToastAndroid.LONG,
		)
	}

	render() {
		return (
			<View style={styles.container}>
				<StatusBar hidden={true} />
				<TouchableNativeFeedback
					onPress={() => this.props.navigation.openDrawer()}
				>
					<Image
						style={{
							height: 35,
							width: 35,
							position: "absolute",
							left: 20,
							top: 20,
						}}
						source={require("../Média/menu-icon.png")}
					/>
				</TouchableNativeFeedback>

				<View style={styles.flex}>
					{list.map((e, i) => {
						return (
							<AwesomeButtonRick
								key={`button_${i}`}
								type="primary"
								style={styles.button}
								onPress={() => this.copy(e)}
							>
								<Text style={styles.Text}>{e}</Text>
							</AwesomeButtonRick>
						)
					})}
				</View>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: "center",
		justifyContent: "center",
		backgroundColor: "#202125",
	},
	Text: {
		margin: 10,
		fontSize: 15,
		color: "black",
		fontFamily: "Manjari-Bold",
	},
	button: {
		margin: 10,
	},
	flex: {
		flexDirection: "row",
	},
})

export default Rigolo
