import React from "react";
import {
    View,
    Text,
    Image,
    Linking,
    StatusBar,
    StyleSheet,
    ScrollView,
    ToastAndroid,
    TouchableNativeFeedback,
} from "react-native";


class Coronavirus extends React.Component {
    render() {
        return (
            <View style={{flex: 1, backgroundColor: "#202125"}}>
                <StatusBar hidden={true}/>

                <TouchableNativeFeedback onPress={() => this.props.navigation.openDrawer()}>
                    <Image
                        style={{height: 35, width: 35, position: 'absolute', top: 25, left: 25}}
                        source={require('../Média/menu-icon.png')}
                    />
                </TouchableNativeFeedback>


                <View 
                    style={{
                        flex: 1,
                        marginTop: 80,
                        flexDirection: "row",
                        justifyContent: "center",
                    }}
                >                    

                    <ScrollView>
                        <View style={{flexDirection: "row", justifyContent: 'space-around', flexWrap: "wrap"}}>
                            <TouchableNativeFeedback onPress={() => ToastAndroid.show("Le m'echant !", ToastAndroid.LONG)}>
                                <Image
                                    style={{height: 75, width: 75, marginTop: 30, marginHorizontal: 10}}
                                    source={require("../Média/coronavirus/Coro-rouge.png")}
                                />
                            </TouchableNativeFeedback>

                            <Image
                                style={{height: 75, width: 75, marginTop: 30, marginHorizontal: 10}}
                                source={require("../Média/coronavirus/Coro-jaune.png")}
                            />

                            <Image
                                style={{height: 75, width: 75, marginTop: 30, marginHorizontal: 10}}
                                source={require("../Média/coronavirus/microbe.png")}
                            />


                            <Image
                                style={{height: 75, width: 75, marginTop: 30, marginHorizontal: 10}}
                                source={require("../Média/coronavirus/tube.png")}
                            />

                            <TouchableNativeFeedback onPress={() => ToastAndroid.show("Reste chez toi !", ToastAndroid.LONG)}>
                                <Image
                                    style={{height: 75, width: 75, marginTop: 30, marginHorizontal: 10}}
                                    source={require("../Média/coronavirus/médoc.png")}
                                />
                            </TouchableNativeFeedback>

                            <Image
                                style={{height: 75, width: 75, marginTop: 30, marginHorizontal: 10}}
                                source={require("../Média/coronavirus/médoc-liquide.png")}
                            />


                            <Image
                                style={{height: 75, width: 75, marginTop: 30, marginHorizontal: 10}}
                                source={require("../Média/coronavirus/microscope.png")}
                            />

                            <Image
                                style={{height: 75, width: 75, marginTop: 30, marginHorizontal: 10}}
                                source={require("../Média/coronavirus/masque.png")}
                            />

                            <Image
                                style={{height: 75, width: 75, marginTop: 30, marginHorizontal: 10}}
                                source={require("../Média/coronavirus/bactérie.png")}
                            />


                            <Image
                                style={{height: 75, width: 75, marginTop: 30, marginHorizontal: 10}}
                                source={require("../Média/coronavirus/perfu.png")}
                            />

                            <TouchableNativeFeedback onPress={() => ToastAndroid.show("Vas te laver les mains !", ToastAndroid.LONG)}>
                                <Image
                                    style={{height: 75, width: 75, marginTop: 30, marginHorizontal: 10}}
                                    source={require("../Média/coronavirus/savon.png")}
                                />
                            </TouchableNativeFeedback>

                            <Image
                                style={{height: 75, width: 75, marginTop: 30, marginHorizontal: 10}}
                                source={require("../Média/coronavirus/loupe.png")}
                            />
                        </View>


                        {/* <View style={{flexDirection: "row", justifyContent: 'space-around', marginTop:30}}>
                            
                        </View> */}
                    </ScrollView>
                
                {/* <Text style={[style.Text, {marginTop: 10}]}>ᓚᘏᗢ</Text> */}

                </View>
            </View>
        )
    }
}


const style = StyleSheet.create({
    Text: {
        fontFamily: "Manjari-Bold",
        color: "#7d9ce8",
        fontSize: 19,
    }
})


export default Coronavirus