import React from "react"
import {
	StyleSheet,
	View,
	Text,
	Image,
	TouchableNativeFeedback,
	ScrollView,
	Animated,
	StatusBar,
	Linking,
	Dimensions,
} from "react-native"
import LinearGradient from "react-native-linear-gradient"
import Sound from "react-native-sound"
import Module, { random } from "../Components/module"

const Déja = { sound: new Sound("deja_vu.mp3"), isPlaying: false }
const boom = { sound: new Sound("boom.mp3"), isPlaying: false, image: require("../Média/boom.png") }

const sncf = { sound: new Sound("sncf.mp3"), soundName: "sncf", isPlaying: false, image: require("../Média/logo-sncf.png"), i: 1 }
const ah = { sound: new Sound("ah.mp3"), soundName: "ah", isPlaying: false, image: require("../Média/ah.png"), i: 2 }
const toca = { sound: new Sound("tocata.mp3"), soundName: "toca", isPlaying: false, image: require("../Média/minions.png"), i: 3 }
const tac = { sound: new Sound("tac.mp3"), soundName: "tac", isPlaying: false, image: require("../Média/clav.png"), i: 1 }
const bull = { sound: new Sound("bull.mp3"), soundName: "bull", isPlaying: false, image: require("../Média/bulle.png"), i: 2 }
const sonnette = { sound: new Sound("sonette.mp3"), soundName: "sonnette", isPlaying: false, image: require("../Média/sonnette.png"), i: 3 }
const tombo = { sound: new Sound("tombo.mp3"), soundName: "tombo", isPlaying: false, image: require("../Média/tambour.png"), i: 1 }
const bol = { sound: new Sound("bol.mp3"), soundName: "bol", isPlaying: false, image: require("../Média/bol.png"), i: 2 }
const marche = { sound: new Sound("marche.mp3"), soundName: "marche", isPlaying: false, image: require("../Média/marche.png"), i: 3 }
const av = { sound: new Sound("avengers.mp3"), soundName: "av", isPlaying: false, image: require("../Média/avengers.png"), i: 1 }
const non = { sound: new Sound("gotaga.mp3"), soundName: "non", isPlaying: false, image: require("../Média/non.png"), i: 2 }
const coucou = { sound: new Sound("coucou.mp3"), soundName: "coucou", isPlaying: false, image: require("../Média/coucou.png"), i: 3 }
const temp = { sound: new Sound("tempate.mp3"), soundName: "temp", isPlaying: false, image: require("../Média/temp.png"), i: 1 }
const rob = { sound: new Sound("roblox.mp3"), soundName: "rob", isPlaying: false, image: require("../Média/roblox.png"), i: 2 }
const prout = { sound: new Sound("prout.mp3"), soundName: "prout", isPlaying: false, image: require("../Média/prout.png"), i: 3 }
const gifi = { sound: new Sound("gifi.mp3"), soundName: "gifi", isPlaying: false, image: require("../Média/gifi.png"), i: 1 }
const sat = { sound: new Sound("saturer.mp3"), soundName: "sat", isPlaying: false, image: require("../Média/sat.png"), i: 2 }
const swing = { sound: new Sound("swing.mp3"), soundName: "swing", isPlaying: false, image: require("../Média/elec.png"), i: 3 }
const death = { sound: new Sound("death.mp3"), soundName: "death", isPlaying: false, image: require("../Média/death.png"), i: 1 }
const popo = { sound: new Sound("popo.mp3"), soundName: "popo", isPlaying: false, image: require("../Média/coronavirus/médoc-liquide.png"), i: 2 }
const shot = { sound: new Sound("headshot.mp3"), soundName: "shot", isPlaying: false, image: require("../Média/headshot.png"), i: 3 }
const verre = { sound: new Sound("verre.mp3"), soundName: "verre", isPlaying: false, image: require("../Média/verre.png"), i: 1 }
const goute = { sound: new Sound("eau.mp3"), soundName: "goute", isPlaying: false , image: require("../Média/goute.png"), i: 2 }
const coq = { sound: new Sound("coq.mp3"), soundName: "coq", isPlaying: false, image: require("../Média/coq.png"), i: 3 }
const poule = { sound: new Sound("poule.mp3"), soundName: "poule", isPlaying: false, image: require("../Média/poulet.png"), i: 1 }
const fox = { sound: new Sound("twentyth_century_fox.mp3"), soundName: "fox", isPlaying: false, image: require("../Média/20th_century_fox.png"), i: 2 }
const engine = { sound: new Sound("engine.mp3"), soundName: "engine", isPlaying: false, image: require("../Média/ferrari.png"), i: 3 }
const universal = { sound: new Sound("universal_studios.mp3"), soundName: "universal", isPlaying: false, image: require("../Média/universal.png"), i: 1 }
const mario = { sound: new Sound("mario.mp3"), soundName: "mario", isPlaying: false, image: require("../Média/level.png"), i: 2 }
const piece = { sound: new Sound("piece.mp3"), soundName: "piece", isPlaying: false, image: require("../Média/piece.png"), i: 3 }


let array = [
	sncf,
	ah,
	toca,
	tac,
	bull,
	sonnette,
	tombo,
	bol,
	marche,
	coucou,
	non,
	av,
	temp,
	rob,
	prout,
	gifi,
	sat,
	swing,
	death,
	popo,
	shot,
	verre,
	goute,
	coq,
	poule,
	fox,
	engine,
	universal,
	mario,
	piece,
]


const fromTopAnimation = new Animated.Value(0)
const fromLeftAnimation = new Animated.Value(Dimensions.get("screen").width)
const fromRightAnimation = new Animated.Value(Dimensions.get("screen").width - Dimensions.get("screen").width * 2)


class Buttons extends React.Component {

	playSound(SoundName) {
		if (SoundName.isPlaying) {
			SoundName.sound.stop()
			SoundName.isPlaying = false
		} else {
			SoundName.isPlaying = true
			
			// //? set correct volume
			// let systemVolume = SoundName.sound.getVolume()
			// console.log(systemVolume)
			// SoundName.sound.setVolume(systemVolume)
			
			SoundName.sound.play(() => {
				SoundName.isPlaying = false
			})
			
		}
	}

	
	componentDidMount() {
		Animated.spring(
			fromTopAnimation, {
				toValue: 100,
				speed: 4,
				bounciness: 20,
				useNativeDriver: true
			}
		).start()

		Animated.spring(
			fromLeftAnimation, {
				toValue: 0,
				speed: 4,
				bounciness: 20,
				useNativeDriver: true
			}
		).start()

		Animated.spring(
			fromRightAnimation, {
				toValue: 0,
				speed: 4,
				bounciness: 20,
				useNativeDriver: true
			}
		).start()
	}
		
	render () {
		return (
			<View style={{backgroundColor: "#202125", flex: 1}}>

				<StatusBar hidden={true} />

				<ScrollView styles={{flex: 1, justifyContent: "center"}}>
					<TouchableNativeFeedback onPress={() => {
						console.log("drawer")
						this.props.navigation.openDrawer()
					}}>
						<Animated.Image
							style={{
								height: 35,
								width: 35,
								top: -80,
								left: 20,
								position: "absolute",
								backgroundColor: "red",
								transform: [{ translateY: fromTopAnimation }]
							}}
							source={require("../Média/menu-icon.png")}
						/>
					</TouchableNativeFeedback>				

					<Animated.View style={{ transform: [ {translateX: fromLeftAnimation} ], justifyContent: "space-evenly" }}>
						<TouchableNativeFeedback
							touchSoundDisabled={true}
							onPress={() => this.playSound(Déja)}
							onLongPress={() => Linking.openURL("https://cjaac.netlify.app")}
						>
							<LinearGradient style={styles.BigButton} start={{x: 0, y: 1}} end={{x: 1, y: 0}} colors={random}>
								<Text style={styles.Text}>SoundBox</Text>
							</LinearGradient>
						</TouchableNativeFeedback>
					</Animated.View>



					<Animated.View style={ {
						transform: [ { translateX: fromRightAnimation } ],
						flexDirection: "row",
						flexWrap: "wrap",
						justifyContent: "space-evenly",
					} }>
							
						{ array.map((v, i) => {
							let start
							let end
							if (v.i === 1) {
								start = {x: 0, y: 0}
								end = {x: 1, y: 1}
							} else if (v.i === 2) {
								start = {x: 0, y: 0}
								end = {x: 0, y: 1}
							} else if (v.i === 3) {
								start = {x: 1, y: 0}
								end = {x: 0, y: 1}
							}
							return <Module key={`item_${v.soundName}`} onPress={() => this.playSound(array[i])} image={v.image} start={start} end={end} />
						}) }
							
						<TouchableNativeFeedback touchSoundDisabled={true} onPress={() => this.playSound(boom)}>
							<LinearGradient colors={random} style={styles.Button}>
								<Image style={{width: 100, height: 90}} source={require("../Média/boom.png")}/>
							</LinearGradient>
						</TouchableNativeFeedback>
					
					</Animated.View>
				</ScrollView>
			</View>
		)
	}
}


const styles = StyleSheet.create({
	Button: {
		elevation: 3,
		marginBottom: 40,
		marginTop: 40,
		alignItems: "center",
		justifyContent: "center",
		shadowOpacity: 1,
		shadowColor: "black",
		backgroundColor: "#7d9ce8",
		padding: 7,
		borderRadius: 20,
		width: 250,
		height: 90,
	},
	Text: {
		fontSize: 30,
		fontFamily: "Manjari-Bold",
		color: "black",
	},
	BigButton: {
		marginLeft: "auto",
		marginRight: "auto",
		marginTop: 140,
		marginBottom: 100,
		elevation: 3,
		color: "black",
		padding: 7,
		paddingBottom: 2,
		borderRadius: 15,
	},
})

export default Buttons