import React from 'react';
import {
    View
} from 'react-native';
import Module from './module';


class Pack extends React.Component {
    render() {
        return(
            <View style={{marginLeft: 100, flexDirection: "row"}}>
                <Module style={this.props.style || {width: 55, height: 55}} start={{x: 0, y: 0}} end={{x: 1, y: 1}} onPress={this.props.onPress} image={this.props.image} />
                <Module style={this.props.style2 || {width: 55, height: 55}} start={{x: 0, y: 0}} end={{x: 0, y: 1}} onPress={this.props.onPress2} image={this.props.image2} />
                <Module style={this.props.style3 || {width: 55, height: 55}} start={{x: 1, y: 0}} end={{x: 0, y: 1}} onPress={this.props.onPress3} image={this.props.image3} />
            </View>
        )
    }
}

export default Pack