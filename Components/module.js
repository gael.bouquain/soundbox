import React from "react"
import { View, Image, StyleSheet, TouchableNativeFeedback, Dimensions } from "react-native"
import LinearGradient from "react-native-linear-gradient"

const linearGradientArray = [
	[ "#003251", "#00415e", "#005069", "#006072", "#006f79" ],
	// ['#4d0076', '#560091', '#5d00ad', '#6106cb', '#6112eb'],
	[ "#760000", "#942208", "#b13d0c", "#cf5810", "#eb7312" ],
	[ "#006c34", "#0a8849", "#13a460", "#19c278", "#1ce191" ],
	[ "#a65400", "#bd7600", "#cf9b00", "#dac200", "#ddeb05" ],
	[ "#370507", "#610414", "#8e0218", "#bc0319", "#eb1212" ],
	[ "#2f7c63", "#299483", "#1caca6", "#09c3cd", "#11dbf5" ],
	[ "#61003a", "#ff0098" ],
	[ "#5d3a08", "#f3ba5c" ],
]

export const random =
	linearGradientArray[Math.floor(Math.random() * linearGradientArray.length)]

class Module extends React.Component {
	render() {
		return (
			<View>
				<TouchableNativeFeedback
					touchSoundDisabled={true}
					onPress={this.props.onPress}
				>
					<LinearGradient
						style={styles.Button}
						start={this.props.start}
						end={this.props.end}
						colors={random}
					>
						<Image
							style={this.props.style || { width: 55, maxHeight: 55 }}
							source={this.props.image}
						/>
					</LinearGradient>
				</TouchableNativeFeedback>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	Button: {
		elevation: 3,
		width: Dimensions.get("window").width / 3 - 30,
		height: Dimensions.get("window").width / 4 - 30,
		marginBottom: 20,
		alignItems: "center",
		justifyContent: "center",
		shadowOpacity: 1,
		shadowColor: "black",
		backgroundColor: "#7d9ce8",
		borderRadius: 20,
	},
})

export default Module
